import {render, screen} from '@testing-library/react'

import Article from '../components/Article'


describe('Article Component', () => {
    test('debería renderizar el título', () => {
        render(
            <Article title="Un artículo"/>
        )

        expect(screen.getByText(/un artículo/i)).toBeInTheDocument()
    })

    test('debería renderizar la descripción', () => {
        render(
            <Article 
                title="Un artículo"
                description="Una descripción"
            />
        )

        expect(screen.getByText(/una descripción/i)).toBeInTheDocument()
    })

    test('debería agregar la clase css icon', () => {
        const { container } = render(
            <Article 
                title="Un artículo"
                description="Una descripción"
                icon="fa-recycle"
            />
        )

        const icon = container.querySelector('i')

        expect(icon).toHaveClass('fa-recycle')
    })
})
