import React from "react";

import img from "../img/tema1_1.jpg";

class ThemeOne extends React.Component {
  render() {
    return (
      <div>
        <h2 className="titulo">Ciclo de vida de un sistema</h2>
        <div className="articulo2">
          <p>
            Es un sistema, automatizado o manual, que engloba a personas,
            máquinas y/o métodos organizados para recopilar, procesar,
            transmitir datos que representan información. Un sistema de
            información engloba la infraestructura, la organización, el personal
            y todos los componentes necesarios para la recopilación,
            procesamiento, almacenamiento, transmisión, visualización,
            diseminación y organización de la información. Cualquier sistema de
            información va pasando por una serie de fases a lo largo de su vida.
            Su ciclo de vida comprende una serie de etapas entre las que se
            encuentran las siguientes:
            <br />
            <br />
            <h2>Planificación</h2>
            Realizar una serie de tareas previas que influirán decisivamente en
            la finalización con éxito del proyecto.
            <br />
            <br />
            <h2>Análisis</h2>
            Averiguar qué es exactamente lo que tiene que hacer el sistema. La
            etapa de análisis en el ciclo de vida del software corresponde al
            proceso mediante el cual se intenta descubrir qué es lo que
            realmente se necesita y se llega a una comprensión adecuada de los
            requerimientos del sistema.
            <br />
            <br />
            <h2>Diseño</h2>
            Se han de estudiar posibles alternativas de implementación para el
            sistema de información que hemos de construir y se ha de decidir la
            estructura general que tendrá el sistema (su diseño arquitectónico).
            El diseño de un sistema es complejo y el proceso de diseño ha de
            realizarse de forma iterativa.
            <br />
            <br />
            <h2>Implementación</h2>
            Seleccionar las herramientas adecuadas, un entorno de desarrollo que
            facilite nuestro trabajo y un lenguaje de programación apropiado
            para el tipo de sistema que vayamos a construir. La elección de
            estas herramientas dependerá en gran parte de las decisiones de
            diseño que hayamos tomado hasta el momento y del entorno en el que
            nuestro sistema deberá funcionar.
            <br />
            <br />
            <h2>Pruebas</h2>
            Tiene como objetivo detectar los errores que se hayan podido cometer
            en las etapas anteriores del proyecto (y, eventualmente,
            corregirlos). La búsqueda de errores que se realiza en la etapa de
            pruebas puede adaptar distintas formas, en función del contexto y de
            la fase del proyecto.
            <br />
            <br />
            <h2>Instalación o despliegue</h2>
            Debemos de planificar el entorno en el que el sistema debe
            funcionar, tanto hardware como software: equipos necesarios y su
            configuración física, redes de interconexión entre los equipos y de
            acceso a sistemas externos, sistemas operativos y bibliotecas. Estas
            etapas son un reflejo del proceso que se sigue a la hora de resolver
            cualquier tipo de problema.
            <br />
            <br />
            <h2>Uso y mantenimiento</h2>
            La etapa de mantenimiento consume típicamente del 40 al 80 por
            ciento de los recursos de una empresa de desarrollo de software. De
            hecho, con un 60% de media, es probablemente la etapa más importante
            del ciclo de vida del software. Eliminar los defectos que se
            detecten durante su vida útil, lo primero que a uno se le viene a la
            cabeza cuando piensa en el mantenimiento de cualquier cosa.
            Adaptarlo a nuevas necesidades cuando el sistema ha de funcionar
            sobre una nueva versión del sistema operativo o en un entorno
            hardware diferente. Añadirle nueva funcionalidad, cuando se proponen
            características deseables que supondrían una mejora del sistema ya
            existente.
          </p>
          <img src={img} alt="" />
        </div>
      </div>
    );
  }
}

export default ThemeOne;
