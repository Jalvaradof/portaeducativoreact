import React from 'react'


class ThemeThree extends React.Component {
    render() {
        return (
            <div>
                <h2 class="titulo">¿Qué es el desarrollo web?</h2>
                <div class="articulo2">
                    <p>Desarrollo web significa construir y mantener sitios web; es el trabajo que tiene lugar en un segundo plano y que permite que una web tenga una apariencia impecable, un funcionamiento rápido y un buen desempeño para permitir la mejor experiencia de usuario. Los desarrolladores web son como duendes con poderes: nunca los ves, pero son los que hacen que todo esté bien y funcione de manera rápida y eficiente.

                    Los conocimientos y habilidades vinculados al desarrollo web son los más demandados y también los mejor pagados. Se trata de una carrera con muchas posibilidades y salidas. Aquí puedes informarte mejor de cómo convertirte en desarrollador·a web y conseguir un título oficial a través del Programa en Desarrollo web – Frontend de OpenClassrooms.  

                    Pero, ¿cómo lograrlo?

                    Los desarrolladores web lo hacen a través de diversos lenguajes de programación. El lenguaje que usan en cada momento depende del tipo de tarea que están haciendo. El desarrollo web se divide, de forma general, en Frontend (la parte cliente) y Backend (la parte servidor).

                    Frontend, Backend o Full-stack, ¿quién es quién?

                    Un desarrollador Frontend se encarga de la composición, diseño e interactividad usando HTML, CSS y JavaScript. El desarrollador Frontend toma una idea y la convierte en realidad. Lo que ves y lo que usas, como por ejemplo el aspecto visual del sitio web, los menús desplegables y el texto, son creados por el desarrollador Frontend, que escribe una serie de programas para dar estructura, forma e interactividad a estos elementos. Estos programas se ejecutan después a través de un navegador.

                    El desarrollador Backend se encarga de lo que no se ve, es decir, dónde se almacenan los datos. Sin datos no hay Frontend. El Backend consiste en el servidor que acoge la web, una aplicación para ejecutarlo y una base de datos. El desarrollador Backend utiliza programas de computación para asegurar que el servidor, la aplicación y la base de datos tengan un desempeño regular conjunto. Además, analiza qué necesita la empresa y proporciona soluciones de programación eficientes. Para hacer este increíble trabajo, utiliza una serie de lenguajes del lado del servidor, como PHP, Ruby, Python y Java.

                    Si te interesan tanto el Frontend como el Backend, deberías plantearte convertirte en desarrollador·a Full-stack. El desarrollador Full-stack está a cargo tanto del Frontend como del Backend, y necesita saber cómo funciona la web a todos los niveles para determinar cómo se van a coordinar la parte cliente y la parte servidor. Para alcanzar este nivel de experiencia hace falta más tiempo, claro, puesto que hay más que aprender.  </p>
                </div>
            </div>
        )
    }
}


export default ThemeThree