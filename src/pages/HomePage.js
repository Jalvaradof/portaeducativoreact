import React from "react";

import Header from "../components/Header";
import AboutUs from "../components/AboutUs";
import Questions from "../components/Questions";
import Footer from "../components/Footer";

class HomePage extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <AboutUs />
        <Questions />
        <Footer />
      </div>
    );
  }
}

export default HomePage;
