import React from 'react'

import logo from '../img/logo_iujo.png'
import principal from '../img/principal.svg'

class Header extends React.Component {
    render() {
        return (
            <div>
                <header>
                    <nav>
                        <section className="contenedor nav">
                            <div className="logo">
                                <img src={logo} alt=""/>
                            </div>
                            <div className="enlaces-header">
                                <a href="!#">Inicio</a>
                            </div>
                            <div className="hamburguer">
                                <i className="fas fa-bars"></i>
                            </div>
                        </section>
                    </nav>
                    <div className="contenedor">
                        <section className="contenido-header">
                            <section className="textos-header">
                                <h1>Portal Educativo Instituto Universitario Jesus Obrero</h1>
                                <b><p>"La única diferencia entre el éxito y el fracaso es la capacidad de actuar."</p></b>
                                <a href="https://webiujocatia.wordpress.com/">Conozcanos</a>
                            </section>
                            <img src={principal} alt=""/>
                        </section>
                    </div>
            </header>
        </div>
        )
    }
}

export default Header