import React from 'react'

import logo from '../img/logo_iujo.png'

class Footer extends React.Component {
    render() {
        return (
            <div>
               <footer>
                    <div className="partFooter">
                        <img src={logo} alt=""/>
                    </div>
                    <div className="partFooter">
                        <h4>Redes sociales</h4>
                        <div className="social-media">
                            <i className="fab fa-facebook-f"></i>
                            <i className="fab fa-twitter"></i>
                            <i className="fab fa-instagram"></i>
                            <i className="fab fa-youtube"></i>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer