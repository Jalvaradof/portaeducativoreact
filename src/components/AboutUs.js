import React from "react";

import Article from "./Article";

import ThemeOne from "../pages/ThemeOne";
import ThemeTwo from "../pages/ThemeTwo";
import ThemeThree from "../pages/ThemeThree";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
class AboutUs extends React.Component {
  render() {
    return (
      <div>
        <section className="about-us">
          <div>
            <h2 className="titulo">Temas a trabajar</h2>
            <div className="contenedor-articulo">
              <Router>
                <div className="container-info">
                  <Switch />
                </div>
                <Article
                  title="Ciclo de vida del Software"
                  description="Es el proceso que se sigue para construir, entregar y hacer evolucionar el software, desde la concepción de una idea hasta la entrega y retiro del sistema"
                  icon="fa-recycle"
                  aos="zoom-in-right"
                  to="/ciclo-de-vida-del-software"
                />
                <Article
                  title="Desarrollo de Aplicaciones Web"
                  description="Desarrollo web entendemos todas las disciplinas involucradas en la creación de sitios web, o aplicaciones que se ejecutan en la web y a las que se accede mediante el navegador."
                  icon="fa-code"
                  aos="zoom-in-right"
                  to="/desarrollo-de-aplicaciones"
                />
                <Article
                  title="Aplicación de Metodología de Trabajo"
                  description="La metodología de trabajo es una herramienta muy potente para definir la pautas y procedimientos de la empresa. Está comprobado, que una metodología de trabajo ayuda a optimizar los recursos de la empresa, mejora la calidad del trabajo, reduce los riesgos de los proyectos, establece prioridades"
                  icon="fa-book"
                  aos="zoom-in-right"
                  to="/aplicaciones-de-metodologías-de-trabajo"
                />
                <Route
                  exact
                  path="/ciclo-de-vida-del-software"
                  component={ThemeOne}
                />
                <Route
                  exact
                  path="/desarrollo-de-aplicaciones"
                  component={ThemeTwo}
                />
                <Route
                  exact
                  path="/aplicaciones-de-metodologías-de-trabajo"
                  component={ThemeThree}
                />
              </Router>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default AboutUs;
