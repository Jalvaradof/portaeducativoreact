import React from 'react'

import segundaIlustracion from '../img/segundaIlustracion.svg'


class Questions extends React.Component {
    render() {
        return (
            <div>
                <section className="questions contenedor">
                    <section className="textos-questions">
                        <h1>Comentarios del Tema</h1>
                        <p>Opiniones y detalles personales de cada tema trabajado</p>
                        <a href="!#">Seguir leyendo</a>
                    </section>
                    <img src={segundaIlustracion} alt="" data-aos="zoom-out-up" data-aos-duration="2000"/>
                </section>
            </div>
        )
    }
}

export default Questions