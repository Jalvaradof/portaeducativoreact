import React from 'react'
import { Link } from 'react-router-dom'

class Article extends React.Component {
    render() {
        const { title, description, icon, aos, to } = this.props
        const className = "fas " + icon
        return (
            <div>
                <article className="articulo" data-aos={aos}>
                    <i className={className}></i>
                    <h3>{title}</h3>
                    <p>{description}</p>
                    <Link to={to}>Seguir Leyendo</Link>
                </article>
            </div>
        )
    }
}

export default Article